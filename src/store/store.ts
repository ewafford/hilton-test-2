import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'

import { rootReducer, initialState  } from './reducer'
import { AppState } from './types'

export const initStore = (_initialState: AppState = initialState) => {
  return createStore(
    rootReducer,
    _initialState,
    composeWithDevTools(applyMiddleware(thunkMiddleware))
  )
}
