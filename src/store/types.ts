export interface Room {
  id: number
  selectedAdults: string
  selectedChildren: string
  enabled: boolean
  showCheck: boolean
}

export interface AppState {
  loading: boolean
  saving: boolean
  error: string
  rooms: Room[]
}

export const LOAD_DATA = 'LOAD_DATA'
export const LOAD_SUCCESS = 'LOAD_SUCCESS'
export const LOAD_NOT_FOUND = 'LOAD_NOT_FOUND'
export const LOAD_FAIL = 'LOAD_FAIL'
export const SAVE_DATA = 'SAVE_DATA'
export const SAVE_SUCCESS = 'SAVE_SUCCESS'
export const SAVE_FAIL = 'SAVE_FAIL'
export const TOGGLE_ROOM = 'TOGGLE_ROOM'
export const UPDATE_ADULTS = 'UPDATE_ADULTS'
export const UPDATE_CHILDREN = 'UPDATE_CHILDREN'


export interface LoadDataAction {
  type: typeof LOAD_DATA
}

export interface LoadSuccessAction {
  type: typeof LOAD_SUCCESS
  payload: { rooms: Room[] }
}

export interface LoadNotFoundAction {
  type: typeof LOAD_NOT_FOUND
}

export interface LoadFailAction {
  type: typeof LOAD_FAIL
}

export interface SaveDataAction {
  type: typeof SAVE_DATA
}

export interface SaveSuccessAction {
  type: typeof SAVE_SUCCESS
}

export interface SaveFailAction {
  type: typeof SAVE_FAIL
}

export interface ToggleRoomAction {
  type: typeof TOGGLE_ROOM
  payload: { id: number, enabled: boolean }
}

export interface UpdateAdultsAction {
  type: typeof UPDATE_ADULTS
  payload: { id: number, value: string }
}

export interface UpdateChildrenAction {
  type: typeof UPDATE_CHILDREN
  payload: { id: number, value: string }
}

export type AppActionTypes = 
  | LoadDataAction
  | LoadSuccessAction
  | LoadNotFoundAction
  | LoadFailAction
  | SaveDataAction
  | SaveSuccessAction
  | SaveFailAction
  | ToggleRoomAction 
  | UpdateAdultsAction 
  | UpdateChildrenAction
