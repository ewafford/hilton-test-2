import { 
  AppState, 
  LOAD_DATA,
  LOAD_SUCCESS,
  LOAD_NOT_FOUND,
  LOAD_FAIL,
  SAVE_DATA,
  SAVE_SUCCESS,
  SAVE_FAIL,
  TOGGLE_ROOM, 
  UPDATE_ADULTS, 
  UPDATE_CHILDREN, 
  AppActionTypes 
} from './types'

export const initialState: AppState = {
  loading: true,
  saving: false,
  error: '',
  rooms: [
    {
      id: 1,
      selectedAdults: '1',
      selectedChildren: '0',
      enabled: true,
      showCheck: false
    },
    {
      id: 2,
      selectedAdults: '1',
      selectedChildren: '0',
      enabled: false,
      showCheck: true
    },
    {
      id: 3,
      selectedAdults: '1',
      selectedChildren: '0',
      enabled: false,
      showCheck: true
    },
    {
      id: 4,
      selectedAdults: '1',
      selectedChildren: '0',
      enabled: false,
      showCheck: true
    }
  ]
}

export const rootReducer = (
  state: AppState = initialState,
  action: AppActionTypes
) => {
  switch (action.type) {
    case LOAD_DATA:
      return {
        ...state,
        loading: true
      }

    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        rooms: action.payload.rooms
      }

    case LOAD_NOT_FOUND:
      return {
        ...state,
        loading: false
      }
    
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        error: 'Loading Failed.'
      }

    case SAVE_DATA:
      return {
        ...state,
        saving: true
      }

    case SAVE_SUCCESS:
      return {
        ...state,
        saving: false,
      }
    
    case SAVE_FAIL:
      return {
        ...state,
        saving: false,
        error: 'Saving Failed.'
      }

    case TOGGLE_ROOM:
      if (action.payload.enabled) {
        return {
          ...state,
          rooms: state.rooms.map(room => 
            (room.id <= action.payload.id && room.id > state.rooms[0].id )
              ? { ...room, enabled: action.payload.enabled } 
              : room
          )
        }
      } else {
        return {
          ...state,
          rooms: state.rooms.map(room => 
            (room.id >= action.payload.id )
              ? { ...room, ...initialState.rooms.find(initialRoom => initialRoom.id === room.id) } 
              : room
          )
        }
      }

    case UPDATE_ADULTS:
      return {
        ...state,
        rooms: state.rooms.map(room => 
          (room.id === action.payload.id) 
            ? { ...room, selectedAdults: action.payload.value } 
            : room
          )
      }

    case UPDATE_CHILDREN:
      return {
        ...state,
        rooms: state.rooms.map(room => 
          (room.id === action.payload.id) 
            ? { ...room, selectedChildren: action.payload.value } 
            : room
          )
      }
    default:
      return state
  }
}
