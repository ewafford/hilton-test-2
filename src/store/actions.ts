import { ThunkAction, ThunkDispatch } from 'redux-thunk'
import { AnyAction } from 'redux'

import { 
  AppState,
  LOAD_DATA,
  LoadDataAction,
  LOAD_SUCCESS,
  LoadSuccessAction,
  LOAD_NOT_FOUND,
  LoadNotFoundAction,
  SAVE_DATA,
  SaveDataAction,
  SAVE_SUCCESS,
  SaveSuccessAction,
  TOGGLE_ROOM, 
  ToggleRoomAction,
  UPDATE_ADULTS, 
  UpdateAdultsAction,
  UPDATE_CHILDREN,
  UpdateChildrenAction,
  Room
} from './types'


export const loadData = (): LoadDataAction => ({
  type: LOAD_DATA
})

export const loadSuccess = (rooms: Room[]): LoadSuccessAction => ({
  type: LOAD_SUCCESS,
  payload: { rooms }
})

export const loadNotFound = (): LoadNotFoundAction => ({
  type: LOAD_NOT_FOUND
})

export const saveData = (): SaveDataAction => ({
  type: SAVE_DATA

})

export const saveSuccess = (): SaveSuccessAction => ({
  type: SAVE_SUCCESS
})


export const toggleRoom = (id:number, enabled: boolean): ToggleRoomAction => ({
  type: TOGGLE_ROOM,
  payload: {id, enabled}
})

export const updateAdults = (id:number, value: string): UpdateAdultsAction => {
  return {
    type: UPDATE_ADULTS,
    payload: {id, value}
  }
}

export const updateChildren = (id:number, value: string): UpdateChildrenAction => {
  return {
    type: UPDATE_CHILDREN,
    payload: {id, value}
  }
}

// thunk action
export const fetchData = (): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
  // Invoke API
  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
    return new Promise<void>((resolve) => {
      dispatch(loadData())
      try {
        const serializedState = localStorage.getItem('rooms')
        if (serializedState === null) {
          dispatch(loadNotFound())
        } else {
          dispatch(loadSuccess(JSON.parse(serializedState)))
        }
      } catch (err) {
        console.error('Save Error', err)
      } finally {
        resolve()
      }
    })
  }
}

export const storeData = (): ThunkAction<Promise<void>, AppState, {}, AnyAction> => {
  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>, getState: () => AppState): Promise<void> => {
    return new Promise<void>((resolve) => {
      dispatch(saveData())
      try {
        const serializedState = JSON.stringify(getState().rooms)
        localStorage.setItem('rooms', serializedState)
        dispatch(saveSuccess())
      } catch (err) {
        console.error('Save Error', err)
      } finally {
        resolve()
      }
    })
  }
}
