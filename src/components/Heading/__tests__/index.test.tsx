import { shallow, mount } from 'enzyme'
import 'jest-styled-components'

import Heading from '../index'
import { ElementOptions, TypeOptions } from '../enums'
import { AlignOptions } from '../../Text/enums';

const children = 'My Test Heading'

describe('Heading using a H1 with a type of Title1', () => {
  const element = ElementOptions.H2
  const type = TypeOptions.Title1

  const component = shallow(
    <Heading element={element} type={type}>
      {children}
    </Heading>,
  )

  it('should contain children', () => {
    expect(
      component
        .find('Heading__StyledHeading')
        .render()
        .text(),
    ).toBe(children)
  })

  it('should have passed props', () => {
    expect(component.prop('type')).toBe(type)
    expect(component.prop('element')).toBe(element)
  })

  it(`should have been rendered in ${type}`, () => {
    expect(component.render().prop('name')).toBe(element)
  })

  it('should match snapshot', () => {
    expect(component).toMatchSnapshot()
  })
})

describe('Heading using a Div with a type of Subtitle', () => {
  const element = ElementOptions.Div
  const type = TypeOptions.Subtitle

  const component = mount(
    <Heading element={element} type={type}>
      {children}
    </Heading>,
  )

  it('should use div', () => {
    expect(component.render().prop('name')).toBe(element)
  })

  it('should have text-transform uppercase', () => {
    expect(component).toHaveStyleRule('text-transform', 'uppercase')
  })

  it('should match snapshot', () => {
    expect(component).toMatchSnapshot()
  })
})

describe('Right aligned Heading using a H4 with a type of Display', () => {
  const element = ElementOptions.H4
  const type = TypeOptions.Display
  const align = AlignOptions.Right

  const component = mount(
    <Heading element={element} type={type} align={align}>
      {children}
    </Heading>,
  )

  it('should use H4', () => {
    expect(component.render().prop('name')).toBe(element)
  })

  it('should be right aligned', () => {
    expect(component).toHaveStyleRule('text-align', 'right')
  })

  it('should match snapshot', () => {
    expect(component).toMatchSnapshot()
  })
})

