import { ReactNode } from 'react'

export interface HeadingProps {
  children: ReactNode
  type?: string
  element?: string
  align?: string
}