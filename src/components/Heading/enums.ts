export enum ElementOptions {
  H1 = 'h1',
  H2 = 'h2',
  H3 = 'h3',
  H4 = 'h4',
  H5 = 'h5',
  H6 = 'h6',
  Div = 'div',
}

export enum TypeOptions {
  Display = 'display',
  Subtitle = 'displaySubtitle',
  Title1 = 'title1',
  Title2 = 'title2',
  Title3 = 'title3',
  Title4 = 'title4',
  Title5 = 'title5',
}

export enum AlignOptions {
  Left = 'left',
  Center = 'center',
  Right = 'right',
}

export enum Tokens {
  WeightHeading = 'weightHeading',
  SizeHeading = 'sizeHeading',
}
