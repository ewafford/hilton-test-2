import * as React from 'react'
import styled from 'styled-components'

import { HeadingProps } from './types'
import { ElementOptions, TypeOptions, AlignOptions, Tokens } from './enums'

const Heading: React.SFC<HeadingProps> = ({
  children,
  type = TypeOptions.Title1,
  element = ElementOptions.H1,
  align = AlignOptions.Left,
}) => {
  const getHeadingToken = (name: Tokens) => ({ type }: {type: TypeOptions}) => {
    const headerTokens = {
      [Tokens.WeightHeading]: {
        [TypeOptions.Display]: 'block',
        [TypeOptions.Subtitle]: '400',
        [TypeOptions.Title1]: '600',
        [TypeOptions.Title2]: '600',
        [TypeOptions.Title3]: '500',
        [TypeOptions.Title4]: '400',
        [TypeOptions.Title5]: '400',
      },
      [Tokens.SizeHeading]: {
        [TypeOptions.Display]: '2.5rem',
        [TypeOptions.Subtitle]: '1.25rem',
        [TypeOptions.Title1]: '1.75rem',
        [TypeOptions.Title2]: '1.375rem',
        [TypeOptions.Title3]: '1rem',
        [TypeOptions.Title4]: '0.875rem',
        [TypeOptions.Title5]: '0.75rem',
      },
    }
  
    return headerTokens[name][type]
  }

  const getAlignToken = () => ({ align }: {align: AlignOptions}) => {
    const alignTokens = {
      [AlignOptions.Left]: 'left',
      [AlignOptions.Right]: 'right',
      [AlignOptions.Center]: 'center',
    }
    return alignTokens[align]
  }
  
  const StyledHeading = styled(({ element: Component, className, children }) => (
    <Component className={className}>
      {children}
    </Component>
  ))`
    font-size: ${getHeadingToken(Tokens.SizeHeading)};
    font-weight: ${getHeadingToken(Tokens.WeightHeading)};
    text-align: ${getAlignToken()};
    color: #222;
    line-height: 1.4;
    margin: 0;
    margin-bottom: 15px;
    flex: 1;
    text-transform: ${({ type }) => type === TypeOptions.Subtitle && 'uppercase'};
  `

  return (
    <StyledHeading type={type} element={element} align={align} >
      {children}
    </StyledHeading>
  )
}

export default Heading