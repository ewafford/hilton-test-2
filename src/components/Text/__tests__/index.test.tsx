import * as React from 'react'
import { shallow, mount } from 'enzyme'
import 'jest-styled-components'

import Text, { getWeightToken, getAlignToken, getSizeToken } from '../index'
import { SizeOptions, ElementOptions, WeightOptions, AlignOptions } from '../enums'

const text = 'My Test Text'

describe('Default Text', () => {

  const component = shallow(
    <Text>
      {text}
    </Text>,
  )

  it('should have default props', () => {
    expect(component.prop('size')).toBe(SizeOptions.Normal)
    expect(component.prop('align')).toBe(AlignOptions.Left)
    expect(component.prop('element')).toBe(ElementOptions.P)
    expect(component.prop('uppercase')).toBe(false)
    expect(component.prop('italic')).toBe(false)
  })

  it('should contain children', () => {
    expect(component.children().text()).toBe(text)
  })

  it('should match snapshot', () => {
    expect(component).toMatchSnapshot()
  })
})

describe('Large, Bold, Right Aligned Text in div', () => {
  const size = SizeOptions.Large
  const el = ElementOptions.Div
  const weight = WeightOptions.Bold
  const align = AlignOptions.Right

  const component = mount(
    <Text element={el} size={size} align={align} weight={weight}>
      {text}
    </Text>
  )

  it('should be large', () => {
    expect(component).toHaveStyleRule('font-size', getSizeToken()({size: SizeOptions.Large}))
  })

  it('should be bold', () => {
    expect(component).toHaveStyleRule('font-weight', getWeightToken()({weight: WeightOptions.Bold}))
  })

  it('should be right aligned', () => {
    expect(component).toHaveStyleRule('text-align', getAlignToken()({align: AlignOptions.Right}))
  })

  it('should match the snapshot', () => {
    expect(component).toMatchSnapshot()
  })
})