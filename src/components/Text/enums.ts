export enum SizeOptions {
  Small = 'small',
  Normal = 'normal',
  Large = 'large',
}

export enum WeightOptions {
  Normal = 'normal',
  Bold = 'bold',
}

export enum AlignOptions {
  Left = 'left',
  Center = 'center',
  Right = 'right',
}

export enum ElementOptions {
  P = 'p',
  Span = 'span',
  Div = 'div',
}