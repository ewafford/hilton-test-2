import * as React from 'react'
import styled from 'styled-components'

import { TextProps } from './types'

import {
  WeightOptions,
  ElementOptions,
  AlignOptions,
  SizeOptions,
} from './enums'


export const getWeightToken = () => ({ weight }: {weight: WeightOptions}) => {
  const weightTokens = {
    [WeightOptions.Normal]: '400',
    [WeightOptions.Bold]: '600',
  }
  return weightTokens[weight]
}

export const getAlignToken = () => ({ align }: {align: AlignOptions}) => {
  const alignTokens = {
    [AlignOptions.Left]: 'left',
    [AlignOptions.Right]: 'right',
    [AlignOptions.Center]: 'center',
  }
  return alignTokens[align]
}

export const getSizeToken = () => ({ size }: {size: SizeOptions}) => {
  const sizeTokens = {
    [SizeOptions.Large]: '1.5rem',
    [SizeOptions.Normal]: '1rem',
    [SizeOptions.Small]: '.75rem',
  }
  return sizeTokens[size]
}

export const StyledText = styled(({ element: TextElement, children, className }) => (
  <TextElement className={className}>
    {children}
  </TextElement>
))`
  font-size: ${getSizeToken()};
  font-weight: ${getWeightToken()};
  color: #000;
  line-height: 1.4;
  text-align: ${getAlignToken()};
  text-transform: ${({ uppercase }) => uppercase && `uppercase`};
  font-style: ${({ italic }) => italic && `italic`};
  margin: 0;
  margin-bottom: 10px;
`

const Text: React.SFC<TextProps> = ({
  size = SizeOptions.Normal,
  weight = WeightOptions.Normal,
  align = AlignOptions.Left,
  element = ElementOptions.P,
  uppercase = false,
  italic = false,
  children,
}: TextProps) => (
  <StyledText
    size={size}
    weight={weight}
    align={align}
    element={element}
    uppercase={uppercase}
    italic={italic}
  >
    {children}
  </StyledText>
)

export default Text