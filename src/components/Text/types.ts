import { ReactNode } from 'react'

export interface TextProps {
  children: ReactNode
  element?: string
  size?: string
  weight?: string
  align?: string
  uppercase?: boolean
  italic?: boolean
}
