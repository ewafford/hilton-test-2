import * as React from 'react'
import styled from 'styled-components'  

import { CardProps } from './types'

const StyledChildWrapper = styled(({ children, className }) => (
  <div className={className}>
    {children}
  </div>
))`
  box-shadow: ${({ disabled }) => disabled && `none` || `0 4px 12px 0 rgba(23, 27, 30, 0.1)`};
  background: ${({ disabled }) => disabled && `#eee` || `#fff`};
  border-radius: 5px;
  border: 1px solid #eee;
  width: 100%;
  display: inline-block;
  margin-bottom: 15px;
`

const StyledCard = styled(({ children, className }) => (
  <div className={className}>
    {children}
  </div>
))`
  width: 100%;
  box-sizing: border-box;
  position: relative;
  padding: 10px 15px;
`

const Card: React.SFC<CardProps> = ({
  disabled = false,
  children,
}) => {

  return (
    <StyledChildWrapper disabled={ disabled }>
      <StyledCard>
        {children}
      </StyledCard>
    </StyledChildWrapper>
  )
}

export default Card
