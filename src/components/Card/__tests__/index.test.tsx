import Card from '../index'
import { shallow, mount } from 'enzyme'
import 'jest-styled-components'

const content = 'My Test Content'

describe('Default Card', () => {

  const component = shallow(
    <Card>
      {content}
    </Card>,
  )

  it('should contain children', () => {
    expect(component
      .find('Card__StyledCard')
      .render()
      .text(),
    ).toBe(content)
  })

  it('should match snapshot', () => {
    expect(component).toMatchSnapshot()
  })
})


describe('Disabled Card', () => {

  const component = mount(
    <Card disabled>
      {content}
    </Card>,
  )

  it('should have disabled styles', () => {
    expect(component).toHaveStyleRule('box-shadow', 'none')
    expect(component).toHaveStyleRule('background', '#eee')
  })

  it('should match snapshot', () => {
    expect(component).toMatchSnapshot()
  })
})
