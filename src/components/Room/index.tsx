import * as React from 'react'
import styled from 'styled-components'

import RoomProps from './types'

import Select from '@components/Select'
import { SelectOptions } from '@components/Select/types'
import { Col } from '@components/Grid'
import Card from '@components/Card' 
import Heading from '@components/Heading'
import Checkbox from '@components/Checkbox'

const RoomHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const Room: React.SFC<RoomProps> = ({
  id,
  selectedAdults, 
  selectedChildren, 
  enabled, 
  showCheck,
  onClickCheckbox,
  onChangeAdult,
  onChangeChildren
}) => {

  const adultOptions: Array<SelectOptions> = [{
    value: '1',
    label: '1',
    disabled: false,
  },
  {
    value: '2',
    label: '2',
    disabled: false,
  }]

  const childOptions: Array<SelectOptions> = [{
    value: '0',
    label: '0',
    disabled: false,
  },
  {
    value: '1',
    label: '1',
    disabled: false,
  },
  {
    value: '2',
    label: '2',
    disabled: false,
  }]

  return (
    <Col key={id}>
      <Card disabled={showCheck ? !enabled : false}>
      <RoomHeader>
        <Heading type='title3'>Room {id}</Heading>
        {showCheck && (
          <Checkbox 
            value='enabled'
            checked={enabled} 
            readOnly={false} 
            name='roomEnabled' 
            onChange={() => onClickCheckbox(id, !enabled)}
          />
        )}
      </RoomHeader>
      <Select 
        name={'adults'} 
        label={'Adult(18+)'} 
        disabled={showCheck ? !enabled : false} 
        value={selectedAdults} 
        options={adultOptions} 
        onChange={(e) => onChangeAdult(id, e.target.value)}/>
      <Select 
        name={'children'} 
        label={'Children(0-17)'} 
        disabled={showCheck ? !enabled : false} 
        value={selectedChildren} 
        options={childOptions}
        onChange={(e) => onChangeChildren(id, e.target.value)}/>
      </Card>
    </Col>
  )
}

export default Room