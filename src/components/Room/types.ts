export default interface RoomProps {
  id: number
  selectedAdults: string
  selectedChildren: string
  enabled: boolean
  showCheck: boolean
  onClickCheckbox: (id: number, enabled: boolean) => void
  onChangeAdult: (id: number, value: string) => void
  onChangeChildren: (id: number, value: string) => void
}