import * as React from 'react'
import { shallow } from 'enzyme'

import Room from '../index'

const id = 1
const selectedAdults = '1'
const selectedChildren = '1'
const showCheck = true
const enabled = true

const mockChangeEnable = jest.fn()
const mockChangeAdult = jest.fn()
const mockChangeChildren = jest.fn()

describe('Room', () => {
  const component = shallow(
    <Room
      id={id}
      selectedAdults={selectedAdults}
      selectedChildren={selectedChildren}
      enabled={enabled}
      showCheck={showCheck}
      onClickCheckbox={mockChangeEnable}
      onChangeAdult={mockChangeAdult}
      onChangeChildren={mockChangeChildren}
    />
  )

  it('should call function on checkbox click', () => {
    const Checkbox = component.find('[name="roomEnabled"]')
    const event = {target: {id: 1, enabled: true}}
    expect(Checkbox).toHaveLength(1)
    Checkbox.simulate('change', event)
    expect(mockChangeEnable).toHaveBeenCalled()
  })

  it('should call function on Adult Select Change', () => {
    const AdultSelect = component.find('[name="adults"]')
    const event = {target: {id: id, value: '1'}}
    AdultSelect.simulate('change', event)
    expect(mockChangeAdult).toHaveBeenCalled()
  })

  it('should call function on Children Select Change', () => {
    const ChildrenSelect = component.find('[name="children"]')
    const event = {target: {id: id, value: '1'}}
    ChildrenSelect.simulate('change', event)
    expect(mockChangeAdult).toHaveBeenCalled()
  })

  it('should match the snapshot', () => {
    expect(component).toMatchSnapshot()
  })
})