import Icon from '../index'
import { shallow } from 'enzyme'

describe('Chevron', () => {

  const component = shallow(
    <Icon name={'chevronDown'}/>
  )

  it('should have name prop', () => {
    expect(component.prop('name')).toBe('chevronDown')
  })


  it('should match snapshot', () => {
    expect(component).toMatchSnapshot()
  })
})

describe('Check', () => {

  const component = shallow(
    <Icon name={'check'}/>
  )

  it('should have name prop', () => {
    expect(component.prop('name')).toBe('check')
  })

  it('should match snapshot', () => {
    expect(component).toMatchSnapshot()
  })
})
