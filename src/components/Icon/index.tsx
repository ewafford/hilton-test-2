import * as React from 'react'
import * as icons from './icons'
import styled from 'styled-components'

import { IconProps } from './types'

const StyledIcon = styled.span<IconProps>`
  width: 1.0rem;
  height: 1.0rem;
  vertical-align: middle;
  display: inline-block;
`

const Icon: React.SFC<IconProps> = ({name}) => {
  const { viewBox, id } = icons[name]
  return (
    <StyledIcon name={name}>
      <svg viewBox={viewBox}>
        <use xlinkHref={`#${id}`} href={`#${id}`} />
      </svg>
    </StyledIcon>
  )
}

export default Icon
