import * as icons from './icons'


export type Icons = typeof icons
export type IconName = keyof Icons

export interface IconProps {
  name: IconName
}
