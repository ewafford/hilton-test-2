import * as React from 'react'
import styled from 'styled-components'

import { StyledSelectProps, SelectProps} from './types'
import Icon from '@components/Icon'

const Label = styled.label`
  position: relative;
  display: block;
  width: 100%;
  margin-bottom: 10px;
`

const FormLabel = styled(({ className, children }) => (
  <span className={className}>
    <span>{children}</span>
  </span>
))`
  display: block;
  font-size: 16px;
  color: ${({ disabled }) => disabled ? '#555' : '#222'};
  line-height: 1.4;
  margin-bottom: 5px;
`

const StyledSelect = styled(
  React.forwardRef<HTMLSelectElement, StyledSelectProps>(
    (
      { className, children, value, disabled, name, onChange },
      ref,
    ) => (
      <select
        className={className}
        value={value}
        onChange={onChange}
        disabled={disabled}
        name={name}
        ref={ref}
      >
        {children}
      </select>
    ),
  ),
)`
  appearance: none;
  background: #fff;
  border-radius: 4px;
  cursor: pointer;
  color: #222;
  font-size: 1rem;
  height: 3rem;
  padding: 10px;
  outline: none;
  width: 100%;
  transition: box-shadow 0.5s ease-in-out;
  z-index: 2;
  > option {
    color: #222;
  }
  &::-ms-expand {
    display: none;
  }
  border: 0;
  box-shadow: inset 0 0 0 1px #222;
  &:focus {
    box-shadow: inset 0 0 0 2px #007bff;
  }
  &:disabled {
    color: #555;
    background: #eee;
    cursor: default;
  }
`

export const SelectContainer = styled(({ className, children }) => (
  <div className={className}>{children}</div>
))`
  position: relative;
  display: flex;
  align-items: center;
  background: #fff;
  width: 100%;
  box-sizing: border-box;
  cursor: pointer;
`

const SelectSuffix = styled(({ children, className }) => (
  <div className={className}>{children}</div>
))`
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 0;
  right: 8px;
  color: #222;
  pointer-events: none;
  z-index: 3;
  height: 100%;
`

const Select = React.forwardRef((props: SelectProps, ref: React.Ref<HTMLSelectElement>) => {
  const {
    className='',
    label,
    placeholder,
    value,
    disabled = false,
    name,
    onChange,
    options,
  } = props

  return (
    <Label>
      {label && (
        <FormLabel disabled={disabled}>
          {label}
        </FormLabel>
      )}
      <SelectContainer disabled={disabled}>
        <StyledSelect
          className={className}
          disabled={disabled}
          value={value == null ? '' : value}
          name={name}
          onChange={onChange}
          ref={ref}
        >
          {placeholder && (
            <option label={placeholder} value=''>
              {placeholder}
            </option>
          )}
          {options.map(option => (
            <option key={`option-${option.value}`} value={option.value} disabled={option.disabled}>
              {option.label}
            </option>
          ))}
        </StyledSelect>
        <SelectSuffix disabled={disabled}>
          <Icon name={'chevronDown'}/>
        </SelectSuffix>
      </SelectContainer>
    </Label>
  )
})

export default Select