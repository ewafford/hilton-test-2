import React, { ReactNode } from 'react'

export interface SelectOptions {
  value: string
  label: string
  disabled?: boolean
}

export interface SelectProps {
  className?: string
  label?: string
  placeholder?: string
  children?: ReactNode
  value: string
  disabled?: boolean
  name?: string
  options: Array<SelectOptions>
  onChange: (e: React.ChangeEvent<HTMLSelectElement>) => void
}

export interface StyledSelectProps {
  className?: string
  children?: ReactNode
  value: string
  disabled?: boolean
  name?: string
  onChange: (e: React.ChangeEvent<HTMLSelectElement>) => void
}
