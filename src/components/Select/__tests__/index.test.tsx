import * as React from 'react'
import { shallow } from 'enzyme'

import Select from '../index'
import { SelectOptions }  from '../types'

const mockChange = jest.fn()
const placeholder = 'Default Placeholder'
const name = 'name'
const label = 'My Test Label'
const options:SelectOptions[] = [
  { value: '1', label: 'Cat' },
  { value: '2', label: 'Dog' },
  { value: '3', label: 'Horse' },
  { value: '4', label: 'Goat' },
  { disabled: true, value: 'disabled', label: 'Disabled Item' },
]

describe('Select', () => {
  const component = shallow(
    <Select
      value='1'
      name={name}
      label={label}
      placeholder={placeholder}
      options={options}
      onChange={mockChange}
    />
  )

  const select = component.find('Select__StyledSelect')

  it('should have a name', () => {
    expect(select.render().prop('attribs').name).toBe(name)
  })

  it('should have options', () => {
    expect(select.children()).toHaveLength(options.length + 1)
  })

  it('should have placeholder', () => {
    expect(select.childAt(0).text()).toBe(placeholder)
  })

  it('should match the snapshot', () => {
    expect(component).toMatchSnapshot()
  })
})