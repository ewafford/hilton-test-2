import { ReactNode } from 'react'

export interface ButtonProps {
  component?: string
  children: ReactNode
  disabled?: boolean
  href?: string
  external?: boolean
  block?: boolean
  width?: number
  onClick: (e: React.MouseEvent<HTMLButtonElement | HTMLAnchorElement>) => void
}
