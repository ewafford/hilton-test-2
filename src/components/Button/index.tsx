import * as React from 'react'
import styled from 'styled-components'

import { ButtonProps } from './types'

export const StyledButton = styled(
  ({
    component,
    block,
    buttonRef,
    ...props
  }) => {
    const Component = component || 'a'
    return (
      <Component
        type='button'
        {...props}
        ref={buttonRef}
      >
        {props.children}
      </Component>
    )
  },
)`
  position: relative;
  display: ${({ href, component }) => (href || component === "a" ? "inline-flex" : "flex")};
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  appearance: none;
  text-decoration: none;
  width: ${({ block, width }) => block ? "100%" : (width && `${width}px`) || "auto"};
  flex: ${({ block }) => (block ? "1 1 100%" : "0 0 auto")};
  height: 44px;
  background: #508ef5;
  color: #fff;
  border: 0;
  border-radius: 4px;
  padding: 0 15px;
  font-size: 1rem;
  cursor: ${({ disabled }) => (disabled ? "default" : "pointer")};
  transition: all 0.25s ease-in-out !important;
  outline: 0;
  opacity: ${({ disabled }) => disabled && '0.3'};
  pointer-events: ${({ disabled }) => disabled && 'none'};
  &:hover {
    background: ${({ disabled }) => !disabled && '#508eff'};
    color: ${({ disabled }) => !disabled && '#fff'};
  }
  &:active {
    ${({ disabled }) => !disabled && 'transform: scale(0.98)'};
    background: ${({ disabled }) => !disabled && '#508e55'};
    color: ${({ disabled }) => !disabled && '#fff'};
  }
`

const StyledButtonContent = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`

const Button = React.forwardRef((props: ButtonProps, ref: React.Ref<HTMLButtonElement>) => {
  const {
    component = 'button',
    children,
    disabled = false,
    block = false,
    width = 0,
  } = props

  return (
    <StyledButton
      {...props}
      block={block}
      component={component}
      disabled={disabled}
      width={width}
      buttonRef={ref}
    >
      <StyledButtonContent>
        {children}
      </StyledButtonContent>
    </StyledButton>
  )
})

export default Button