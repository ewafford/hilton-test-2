import * as React from 'react'
import { shallow } from 'enzyme'

import Button from '../index'

const children = 'button'
const onClick = jest.fn()

describe('Button', () => {
  const ref = React.createRef<HTMLButtonElement>()
  const component = shallow(
    <Button onClick={onClick} ref={ref}>
      {children}
    </Button>,
  )

  const button = component.find('Button__StyledButton')
  it('should contain text', () => {
    expect(button.render().text()).toBe(children)
  })

  it('should execute onClick method', () => {
    button.simulate('click')
    expect(onClick).toHaveBeenCalled()
  })
  
  it('should have ref', () => {
    expect(ref.current).toBeDefined()
  })

  it('should match snapshot', () => {
    expect(component).toMatchSnapshot()
  })
})
