import styled from 'styled-components'

const Col = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  @media (min-width: 768px) {
    flex-basis: 0;
    flex-grow: 1;
    max-width: 100%;
  }
`

export default Col 