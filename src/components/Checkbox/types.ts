import * as React from 'react'

export interface CheckboxProps {
  name: string
  value: string
  label?: string
  disabled?: boolean
  checked?: boolean
  readOnly?: boolean
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}