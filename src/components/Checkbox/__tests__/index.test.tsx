import * as React from 'react'
import { shallow } from 'enzyme'

import CheckBox from '../index'

const label = 'Checkbox'
const onChange = jest.fn()
const value = 'option'
const name = 'name'

describe(`Default Checkbox`, () => {
  
  const component = shallow(
    <CheckBox
      label={label}
      onChange={onChange}
      value={value}
      name={name}
    />,
  )

  const checkbox = component.find('Checkbox__Input')

  it('should contain a label', () => {
    expect(
      component
        .find('Checkbox__LabelText')
        .render()
        .text(),
    ).toBe(label)
  })

  it('should have a value that matches prop', () => {
    expect(checkbox.prop('value')).toBe(value)
  })

  it('should have a name', () => {
    expect(checkbox.render().prop('attribs').name).toBe(name)
  })

  it('should execute the onChange method', () => {
    checkbox.simulate('change')
    expect(onChange).toHaveBeenCalled()
  })

  it('should match the snapshot', () => {
    expect(component).toMatchSnapshot()
  })
})