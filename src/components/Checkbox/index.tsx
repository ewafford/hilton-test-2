import * as React from 'react'
import styled from 'styled-components'
import { CheckboxProps } from './types'
import Icon from '@components/Icon'
import { StyledText } from '@components/Text'

const IconContainer = styled.div`
  position: relative;
  box-sizing: border-box;
  flex: 0 0 auto;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #fff;
  height: 20px;
  width: 20px;
  border-radius: 4px;
  transform: scale(1);
  transition: all 0.15s ease-in-out;
  & svg {
    visibility: hidden;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 16px;
    height: 16px;
  }
`

const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 0 0 0.5rem;
  flex: 1;
`

const LabelText = styled.span`
  font-weight: 400;
  font-size: 0.75rem;
  color: #222;
  line-height: 20px;
  ${StyledText} {
    font-weight: 400;
    font-size: 0.75rem;
    color: #222;
    line-height: 20px;
  }
`

const Input = styled.input`
  opacity: 0;
  z-index: -1;
  position: absolute;
  &:checked ~ ${TextContainer} > ${LabelText} {
    font-weight: 500;
    & > ${StyledText} {
      font-weight: 500;
    }
  }
  &:checked + ${IconContainer} svg {
    visibility: visible;
  }
  &:focus + ${IconContainer} {
    border: 2px solid #007bff;
  }
  &:active + ${IconContainer} {
    border-color: ${({ disabled }) => disabled ? '#222' : '#007bff'};
    transform: ${({ disabled }) => !disabled && 'scale(0.90)'};
  }
`

export const Label = styled(({ className, children }) => (
  <label className={className}>
    {children}
  </label>
))`
  display: flex;
  width: 100%;
  flex-direction: row;
  flex: 0;
  align-items: self-start;
  margin-bottom: 15px;
  opacity: ${({ disabled }) => (disabled ? '0.5' : '1')};
  cursor: ${({ disabled }) => (disabled ? 'not-allowed' : 'pointer')};
  position: relative;
  ${IconContainer} {
    color: #007bff;
    border: 1px solid #222;
  }
  &:hover ${IconContainer} {
    border-color: ${({ disabled }) => !disabled && '#7f91a8'};
  }
`

const Checkbox = React.forwardRef((props: CheckboxProps, ref: React.Ref<HTMLInputElement>) => {
  const {
    label = '',
    value,
    disabled = false,
    checked = false,
    readOnly = false,
    name,
    onChange,
  } = props

  return (
    <Label disabled={disabled} checked={checked}>
      <Input
        value={value}
        type='checkbox'
        disabled={disabled}
        name={name}
        checked={checked}
        onChange={onChange}
        ref={ref}
        readOnly={readOnly}
      />
      <IconContainer>
        <Icon name={'check'} />
      </IconContainer>
      {label && (
        <TextContainer>
          {label && <LabelText>{label}</LabelText>}
        </TextContainer>
      )}
    </Label>
  )
})

export default Checkbox