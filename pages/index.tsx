import React from 'react'
import { connect } from 'react-redux'
import { ThunkDispatch } from 'redux-thunk'
import { AppState } from '@store/types'
import { fetchData, storeData, toggleRoom, updateAdults, updateChildren } from '@store/actions'

import GlobalStyle from '@common/globals'
import Heading from '@components/Heading'
import { Container, Row, Col } from '@components/Grid'
import Button from '@components/Button'
import Room from '@components/Room'


interface DispatchProps {
  fetchData: () => void
  storeData: () => void
  toggleRoom: typeof toggleRoom
  updateAdults: typeof updateAdults
  updateChildren: typeof updateChildren
}


class Page extends React.Component<AppState & DispatchProps> {

  componentDidMount () {
    this.props.fetchData()
    console.log(this.props.rooms)
  }

  save = () => {
    this.props.storeData();
  }

  toggleRoom = (id:number, enabled: boolean) => {
    this.props.toggleRoom(id, enabled)
  }

  updateAdults = (id:number, quantity: string) => {
    this.props.updateAdults(id, quantity)
  }

  updateChildren = (id:number, quantity: string) => {
    this.props.updateChildren(id, quantity)
  }

  render() {
    return (
      <>
      <GlobalStyle />
      <Container>
        <Row>
          <Col>
            <Heading type={'display'} align={'center'}>
              Hilton Room Selector
            </Heading>
          </Col>
        </Row>
        {this.props.loading ? (
          <Row>
            <Heading type={'display'} align={'center'}>
              Loading...
            </Heading>
          </Row>
        ) : (
          <>
            <Row>
              {this.props.rooms.map(room => 
                <Room
                  key={room.id}
                  {...room}
                  onChangeAdult={this.updateAdults}
                  onChangeChildren={this.updateChildren}
                  onClickCheckbox={this.toggleRoom}
                />
              )}
            </Row>
            <Row>
              <Col>
                <Button onClick={this.save} disabled={this.props.saving}>Save</Button>
              </Col>
            </Row>
          </>
        )}
      </Container>
    </>
    )
  }
}

const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, any>): DispatchProps => ({
  fetchData: async () => {
    await dispatch(fetchData())
  },
  storeData: async () => {
    await dispatch(storeData())
  },
  toggleRoom: (id, enabled) => dispatch(toggleRoom(id, enabled)),
  updateAdults: (id, quantity) => dispatch(updateAdults(id, quantity)),
  updateChildren: (id, quantity) => dispatch(updateChildren(id, quantity))
});


export default connect(state => state, mapDispatchToProps)(Page)