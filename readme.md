# Hilton Test 2

This repo is a sample project submitted by Eric Wafford for consideration by Hilton for a lead development position.

**Technologies Used**

- Next 8
- Typescript
- Styled Components
- Redux for State Management
- TSLint for Code Linting
- Prettier for Code Style Standards
- Jest + Enzyme for Unit Testing

## Setup

In order to compile and run this project, you must first install the dependencies by running the following command:

```sh
$ yarn install
```

### Development

To start the project's development server (defaults to [http://localhost:3000](http://localhost:3000)), run the following command:

```sh
$ yarn dev
```

### Production

To run the project in production mode, make sure to run the build command first:

```sh
$ yarn build
```

And then to start the project with a provided port number (defaults to 3000 if not provided):

```sh
$ PORT=8000 yarn start
```

### Static

To export this project to a static website, use the following command:

```sh
yarn export
```

This will render static HTML pages into `./out`

### Linters

This project provides two linters, one for Typescript and one for type safety checking. Either can be invoked with the following commands:

```sh
$ yarn lint:ts
$ yarn lint:types
```

### Tests

To execute a run of all tests, use the following command:

```sh
$ yarn test
```

